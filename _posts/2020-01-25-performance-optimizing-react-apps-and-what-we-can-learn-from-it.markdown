---
layout:      post
title:       "Performance optimizing React apps and what we can learn from it"
description: "Sooner or later, every major React project will face performance issues, although the whole React library itself is all about fast and optimized UI rendering."
date:        2020-01-25 15:09:00 +0100
---
Sooner or later, every major React project will face performance issues, although the whole React library itself is all about fast and optimized UI rendering. So the performance issues we face in our projects must be all solvable by optimizing our native, self-written JavaScript alone, right?

Well, thats kind of true, but why on earth is React giving us tools like `React.memo` then? Does it solve **our problems** or does it help solving **React's problems**? Let's find out!

## Treating React apps as "functionalish" environments

In a "functionalish" environment (as opposed to *pure* functional environments like Haskell provides), every function returns **always** the same output, when given the same input. And with React you are approaching a functionalish environment, because that is what React encourages you to.

You also have to know, that a React *component* is a function that returns a React *element*. The parameters of these functions are called *props*.

Putting this knowledge together, we are able to make the following statement:

**Every React component returns the same React element, when given the same props.**

Now that would be true, if we would only talk about stateless components. And to keep it simple for now, we will exactly do that.

Given this statement is true, a very interesting questions pops up: can we prevent executing those components, if we see that no prop has changed?

That would be great! Instead of running through each components render function, when *something* changes, we just have to take a look at the components props. The only case where we won't like to do it, is when the component itself is so small and simple, that looking at the props would be more resource heavy than just executing the component.

But how would we do that?

## Primitive types vs. reference types

Before we move on, we must take a look on comparing those two traits of types. While more or less every common, dynamically typed programming language is handling them in kind of the same way, we will especially take a look at the way JavaScript (JS) handles them.

According to [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures) and the there quoted ECMAScript standard, JS only knows about **8** different types:

- `Boolean`
- `Null`
- `Undefined`
- `Number`
- `BigInt`
- `String`
- `Symbol`
- `Object`

Only the latter (`Object`) is a reference type here, that also enables some of the other "compound" types like `Array` and functions (which are just *callable* objects).

You can safely use the comparison operator with the primitive types:

```js
true === true // true
5 > 10 // false
'Hello' == 'Hello' // true
Symbol('hello') === Symbol('hello') // false
```

and so on. You could also compare reference typed values, **but** you won't compare the values itself, but their memory address, which *reference* stands for:

```js
const a = { a: 'a' }
const b = { a: 'a' }

a === a // true
a === b // false
{ a: 'a' } === { a: 'a' } // false
```

## React.memo

To create our "component-execution-optimize-mechanism" we could just compare primitive typed props in their "natural" way.

```js
let lastProps = {}
let lastElement

const memo = Component => (props) => {
  const keyIsTheSame = key => props[key] === lastProps[key]
  const someKeyIsTheSame = Object.keys(props).some(keyIsTheSame)

  if (!someKeyIsTheSame) {
    lastProps = props
    lastElement = Component(props)
  }

  return lastElement
}
```

And would just have defined our own `React.memo` implementation!

The problem here are the reference typed props. We could use `JSON.stringify`, the [deep-diff](https://www.npmjs.com/package/deep-diff) package or some computer science magic to make those comparable, but besides the fact that this could result into very bad performance, at the latest when we have to compare a function, we will fail.

How does the React implementation solve this problem? **It doesn't.** And it tells you about that: [https://reactjs.org/docs/react-api.html#reactmemo](https://reactjs.org/docs/react-api.html#reactmemo)

> By default it will only shallowly compare complex objects in the props object. If you want control over the comparison, you can also provide a custom comparison function as the second argument.

The second argument to `React.memo` is only about breaking referenced typed props down into primitive typed ones, so that we can apply our `memo` function above.

You see that React by itself doesn't do this performance optimizations for executing component functions, because it can't. But it provides you some generic functions you can use to **tell** React how it could apply those optimizations.

### React.useMemo

I told you before, that we've simplified our `memo` use case by only using stateless components. And if you've read the [API doc](https://reactjs.org/docs/react-api.html#reactmemo) about `React.memo` then you know, that `React.memo` should only be used for this case. If you have a *stateful* component, you won't get this optimization.

But for this case (and to not fall into the [React Context API caveat](https://reactjs.org/docs/context.html#caveats)), React gives you another generic function you can use, that is called `useMemo`. And to implement that, we won't have to do anything, because we already did it with our `memo` implementation.

The only difference is, that `useMemo` doesn't have a function parameter to break down reference typed props, it forces you do that by your own and provide every "prop" (we don't have props here, because we don't optimize only components anymore) by a dependency array as the second argument to it. And luckily I already wrote about those dependency arrays here: [/2020/01/05/do-not-think-in-lifecycles-while-using-react-hooks.html](/2020/01/05/do-not-think-in-lifecycles-while-using-react-hooks.html)

When you put this knowledge together, you can implement `memo` just as a higher-order function that uses `useMemo`:

```js
const memo = (Component, breakDownCallback) => props => useMemo(
  () => Component(props),
  [
    ...props,
    breakDownCallback(props),
  ],
)
```

*`useMemo` here uses **nearly** the same function body as our previous `memo` implementation, but with this head/signature: `const useMemo = (callback, dependencies) => element`.*

The advantage of `useMemo` is, that you could put anything into it. And this is exactly how you use it. The only question you have to ask yourself before using `useMemo` is, if the `useMemo` usage would produce **more** overhead than the function you gave it would do alone. If the answer is *no*, then you can safely use `useMemo`. Is the answer *yes*, then just execute your function/logic directly, there is now way to optimize that (only with code optimization itself).

## Applying these functions

You would now be able to optimize stateless and stateful components with these two functions. There is also `useCallback` which is just a slightly different approach to `useMemo`: [https://reactjs.org/docs/hooks-reference.html#usecallback](https://reactjs.org/docs/hooks-reference.html#usecallback).

Optimizing class components uses the same logic, but different functions/classes, which I don't cover here, because there isn't any new knowledge to learn from it. But for completion I link the docs to them here:

- [shouldComponentUpdate()](https://reactjs.org/docs/react-component.html#shouldcomponentupdate)
- [React.PureComponent](https://reactjs.org/docs/react-api.html#reactpurecomponent)

## What we can learn here in general

Optimzing UI rendering with React isn't that hard, as you can see. The basic problem we are facing in all those examples is the *primitive vs reference type* problem, we already covered above and which is not a React problem nor our code's problem neither.

The main takeaway when crafting new components and logic for them: **think about what you put into them as dependencies.**

Don't throw your whole user object into every component that wants to know if the user is currently logged in. Create a primitive, boolean `isLoggedIn` prop, that is easy to compare.

When you work this way, you can take all benefits of `useMemo` and your React app will shine in a new light, performance wise.
