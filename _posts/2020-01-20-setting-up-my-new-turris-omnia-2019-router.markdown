---
layout:      post
title:       "Setting up my new Turris Omnia 2019 router"
description: "Received my new Turris Omnia router and want to set it up, because my currently used FritzBox goes out of support."
date:        2020-01-20 23:37:00 +0100
---
Since my colleague introduced me to the [Indiegogo campagne of the Turris Omnia router](https://igg.me/at/turris-omnia/x#/) back in 2016 I wanted one of them, because they looked so interesting and playful to me, a guy that only has known the standard FritzBoxes provided by the ISP.

"Fortunately", I received a call from my ISP a few days ago, where an employee told me that my currently provided and used FritzBox will go out of support and won't receive any updates by AVM (the manufacturer). Time to get a new one by my own!

## Ordering (I need a DSL modem)

So ordering seems to only be available via Amazon. If you have read the [router specs](https://www.turris.cz/en/omnia/), then you will notice that there is only one SFP slot available to connect to the outer, non-Ethernet world. And FTTH (fiber-to-the-home) is something I've never seen before in Germany. A quick lookup told me, that there are some ISPs that are currently building small FTTH projects for some areas, but nothing I could benefit from.

So what I needed to communicate with my ISP is a DSL modem. One that is VDSL2 compliant. My choice was the [DrayTek Vigor 130](https://www.draytek.com/products/vigor130/), because most of the time I was researching for good DSL modems, this model popped up.

Unfortunately, I also need a USB-C-to-Ethernet adapter for my ultrabook to configure the modem and the router. So, keep that in mind if you are also in that position!

### Unboxing

I love unboxing. I don't order many things, but when I do, I like to keep the boxing as in tact as possible.

![The boxed Turris Omnia](/assets/2020-01-20-setting-up-my-new-turris-omnia-2019-router/boxed-turris-omnia-router.jpeg)

![The unboxed Turris Omnia](/assets/2020-01-20-setting-up-my-new-turris-omnia-2019-router/unboxed-turris-omnia-router.jpeg)

## Connecting to the ISP

Now it's time to establish a connection. I had to use the modem for this purpose, so I must connect my laptop to the modem and configure it properly. Running this device as a modem is pretty simple, but due to my lack of basic "network" knowledge (and with basic I mean things like Wi-Fi standards) I had to read up some of the pretty standard terms.

What the modem does is encoding the signal of the telephone line (which DSL uses) and modulate it to something my router can understand and use. The problem here is, that I have to know how exactly I can receive a signal. There is a webpage at **http://192.168.1.1/** where I can configure [PPPoE](https://en.wikipedia.org/wiki/Point-to-Point_Protocol_over_Ethernet) to dial up into the network of my provider, but that's maybe something my router should take care of? I had to lookup all these questions and this is where the value of this blog post lies.

In the manual of the modem is a page that says how to run the modem in bridged mode. And after some research done in DuckDuckGo I came to the point where I knew that that's the thing I needed. So disabling the whole PPPoE thing and enabling [MPoA](https://en.wikipedia.org/wiki/Multiprotocol_Encapsulation_over_ATM) in bridge mode is exactly what turns the device into a modem. Otherwise the device would be a router by itself, because dialing into the ISPs network is what routers do.

So that's the whole thing. Buuut, I haven't seen any connection in the online status page of the modem, because I haven't set any VLAN tag yet. Seeing that there are **4095** different possibilities to match the correct VLAN tag wasn't that great, but I've learned that the VLAN tag (and also the [MTU](https://en.wikipedia.org/wiki/Maximum_transmission_unit)) is something your ISP has to tell you. Fortunately, my ISP has put that information onto his website.

Connection established! I see some basic information about the connection and that it get's forwarded to whatever device is connected to the modem. And that's the Turris Omnia router now.

## Hello Turris

Plugged the router into the modem and connected my laptop to it. Setting up the device was pretty simple in comparison to the modem. The Turris Omnia comes with a wizard-like setup and asks you for everything a router does.

Here I've seen the PPPoE thing again and gave it the right credentials to connect to the internet. I've enabled Wi-Fi and all the other stuff I needed like LAN-wide DNS and NTP sources. It was time to unplug my laptop and use the Wi-Fi connection. Works like a charm!

But after some speed tests I've faced a new issue. I could only reach a speed peak of 6 MBit/s downstream with my 50 MBit/s plan. It wasn't like this before. Back to the router's config page at **http://192.168.1.1/** I've learned about the **two** UIs that I could use for configuring the router. The basic one is called Foris and the advanced one is called LuCI UI.

I've found the solution to my speed problem in the selection of the Wi-Fi standard ([IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11)). I learned about them, selected the right one (**802.11n** in my case) and was back again with a proper downstream speed.

Great! That's everything I need for now.

## Conclusion

My first impression is pretty neat. When I surfed through the LuCI UI pages I could already feel the GNU/Linux environment the router is powered by ([OpenWrt](https://openwrt.org/)). I even have SSH access to it and can do anything I like, but at least I can do the stuff that is described at the routes homepage.

![The unboxed Turris Omnia](/assets/2020-01-20-setting-up-my-new-turris-omnia-2019-router/final-turris-omnia-setup.jpeg)

PS: In my first night with the router in the same room as me, I noticed that the LED lights are far too bright, to get a good sleep. I haven't found any option to dim them via LuCI UI, but maybe I can handle it directly via SSH. One of the many things I will try out in the next few weeks. For now, a box around it seems to work well enough. :)

PS *Jan 25, 2020*: There is wonderful documentation for all tweaks to the LEDs available here: [https://doc.turris.cz/doc/en/howto/led_settings](https://doc.turris.cz/doc/en/howto/led_settings)

PS *Feb 09, 2020*: I've faced an issue regarding the WAN connection. Fortunately, there is a very active community around in a dedicated forum where I can ask some questions: [https://forum.turris.cz/t/turris-omnia-4-drops-wan-occasionally/12248](https://forum.turris.cz/t/turris-omnia-4-drops-wan-occasionally/12248)
