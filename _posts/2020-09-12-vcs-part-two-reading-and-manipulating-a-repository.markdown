---
layout:      post
title:       "VCS part two: Reading and manipulating a repository"
description: "A repository is a linked list made of commits and you need some commands/functions to work with it."
date:        2020-09-12 22:20:00 +0200
---
In [VCS part one: Writing a small version control system](/2020/09/12/vcs-part-one-writing-a-small-vcs.html) of this series, we created a complete repository. But now we need some way to work with it. We can split the set of commands (functions in our JS example) into two subsets: reading and manipulating ones. Lets grind down the boring, reading ones to head over to the more interesting, manipulating ones.

## Navigating a repository (refreshing CS knowledge)

Before we can create any of those functions, we have one more thing to do (I do that very often, hm?). And that is navigating through a repository. In the first part of this series, we learned, that a repository is just a regular linked list made of commits.

Navigating linked lists is done by good ol' `while` loops:

```js
let co = repository.HEAD

while (co !== null) {
  // Do your stuff
  co = co.parent
}
```

You look at the parent of a commit, decide if it's a valid commit (i.e. not `null`) and then you look at the next parent. Easy.

We can now create a basic function we will use heavily in this part:

```js
function getCommitByHash(hash) {
  let co = repository.HEAD

  while (co !== null) {
    if (co.hash === hash) {
      break
    }

    co = co.parent
  }

  return co
}
```

## Reading functions

With this knowledge, every kind of reading function can be achieved.


### Displaying a commit

Lets create our first function: `show`. Without any argument, it will show you the most current commit of your repository. You could also give it a commit hash, a [revision selection](https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection) or a branch name (while we will read more about branches in the next section, we drop the revision selection one). We name any of those possible arguments a **ref**.

```js
function show(ref) {
  if (ref === undefined) {
    console.log(repository.HEAD)
  } else if (ref is a branch) {
    // You can do this later by your own; it's super easy, trust me
  } else {
    const co = getCommitByHash(ref)

    if (co === null) {
      throw new Error(`ref "${ref}" is not a known ref in this repository.`)
    }

    console.log(co)
  }
}
```

### Logging the commit history

The possible most simple reading function is `log`. I will just show you one variant with no arguments:

```js
function log() {
  let co = repository.HEAD

  while (co !== null) {
    console.log(co)

    co = co.parent
  }
}
```

## A quick introduction to branches

Before we can do any kind of manipulation, we would be better off using branches. With branches we're able to do work aside the "main" commit history, without being scared to destroy something of it.

Branches work nearly in the same way our `HEAD` variable and `parent` commit property works. It is a pointer, that points to a commit. With that in mind we can create multiple pointers with totally different commits that they are pointing to.

```js
const repository = {
  HEAD: ...
}

repository.branches = {
  master: repository.HEAD.parent,
  develop: repository.HEAD,
}
```

## Manipulating functions

The first few manipulating functions we will look at, are the branch handlers.

### Creating new branches

The `branch` function creates new branches that are pointing to the commit the `HEAD` is currently pointing at.

```js
function branch(name) {
  repository.branches[name] = repository.HEAD
}
```

### Checking out branches

The commit the `HEAD` points to is the one *we have checked out*. We can `checkout` a branch by simply changing the `HEAD`:

```js
function checkout(ref) {
  const target = repository.branches[ref] || getCommitByHash(ref)

  if (!target) {
    throw new Error(`ref "${ref}" is not a known ref in this repository.`)
  }

  repository.HEAD = target
}
```

### Reset a branch to a different state

With `reset` you can manipulate your branch to point to a different commit than it did before. For me a better suited name would be just `set`, but here we go:

```js
function reset(ref) {
  const target = repository.branches[ref] || getCommitByHash(ref)

  if (!target) {
    throw new Error(`ref "${ref}" is not a known ref in this repository.`)
  }

  repository.branches[name] = target
}
```

## Manipulating history

After we've looked at the manipulation of branches we will finally look at the manipulation of the history itself via two functions: `merge` and `rebase`. But to keep it simple, I put them into their own [VCS part three: Merging and rebasing branches](/2020/09/12/vcs-part-three-merging-and-rebasing-branches.html).
