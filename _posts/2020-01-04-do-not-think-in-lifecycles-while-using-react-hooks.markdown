---
layout:      post
title:       "Do not think in lifecycles while using React hooks"
description: "If you have done a lot of work with React before hooks arrived then you might think in a different way the hooks want you to."
date:        2020-01-05 01:05:00 +0100
---
It's been a while since I first came into contact with React hooks and a lot of pretty good articles have been created since then. But there is one thing I noticed when working with other people that have made a similiar transition from class components to hooks, that I haven't read about before and that I want to share now.

## Coming from class components

If you have worked with React class components before, then you know about the lifecycle methods that you can use to call different logic depending on where/when in the rendering cycle the mounted component currently is. Namely these functions were:

- `componentWillMount`
- `componentDidMount`
- `componentWillReceiveProps`
- `componentWillUpdate`
- `componentDidUpdate`
- `componentWillUnmount`

You could also throw in some of the other methods like `render` because they also fit into the criteria of a lifecycle method, but I want to focus on the methods in the list above in this article.

Please keep in mind that React has already marked some of them as deprecated, because they don't fit into the **functional** model that is the vision for the library (you will see later why). With React 16, React has been built with a whole new concept/architecture called React Fiber. It enables many of the modern features of the library and will also enable many more in React 17 and beyond.

Now what you might notice about these methods is their name. Even if you have never worked with them or React itself, you can fairly guess what they do and/or when they are getting called, as long as you just know what the React library is built for (rendering an UI depending on a state). They are semantically named.

Just look at this class component example and how it is using the lifecycle methods to do some side effect (it updates the title of the website where the component is mounted and reverts it on unmount):

```jsx
class Title extends Component {
  constructor() {
    this.initialTitle = document.title
  }

  componentDidMount() {
    document.title = this.props.title
  }

  componentDidUpdate() {
    document.title = this.props.title
  }

  componentWillUnmount() {
    document.title = this.initialTitle
  }

  render() {
    return null
  }
}
```

It is completely clear what this class is doing. Especially if you are coming from an OOP background you maybe think this way:

1. `instance.initialTitle` gets initialised inside the constructor, when a class instance is created
2. `instance.componentDidMount` is called afterwards
3. `instance.componentDidUpdate` is called everytime "something" changes
4. `instance.componentWillUnmount` is called when the instance gets destroyed/unmounted

## Transition into functional components

When you now look at functional components and the hooks approach you can't see what is going on that easy. *You have to know it.*

```jsx
const Title = ({ title }) => {
  useEffect(() => {
    const initialTitle = title

    return () => {
      document.title = initialTitle
    }
  }, [])

  useEffect(() => {
    document.title = title
  }, [title])

  return null
}
```

But that is exactly the faulty learning of the transition between those two concepts. It is not anymore about some **life**cycles of a class instance. It is about a function that is getting called **everytime** "something" changes. So you don't have to think about telling React when it should call a method of your class instance, instead you must think about triggering changes in dependencies.

And here the second parameter to the variants of `useEffect` and `useMemo` is the key.

## Taking dependencies into account

The React docs are explaining the technical implementation behind dependency arrays very well: [https://reactjs.org/docs/hooks-effect.html#tip-optimizing-performance-by-skipping-effects](https://reactjs.org/docs/hooks-effect.html#tip-optimizing-performance-by-skipping-effects)

However, what they don't explain very well (at least in my opinion) is how to plan/use them carefully in your software architecture. They are not just a performance optimization, they are the semantically replacements of the previous listed lifecycle methods. **All** of them, the already deprecated ones **and** the "still" used ones, with just one array.

And I bet that if you look on them with this knowledge in mind, you are able to plan your hooks way more careful than you did before. Instead of telling React when to do something based on time, you tell it when to do something based on data. Now find out what data is crucial to your side effect and let React do the work. Don't be stupid and craft a lifecycle in your architecture which you won't need.
