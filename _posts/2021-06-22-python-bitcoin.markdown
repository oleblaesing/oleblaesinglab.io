---
layout:           external
title:            "A from-scratch tour of Bitcoin in Python"
date:             2021-06-22 19:20:00 +0200
external:         https://karpathy.github.io/2021/06/21/blockchain/
external_domain:  karpathy.github.io
---
