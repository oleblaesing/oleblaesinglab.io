---
layout:      post
title:       "Why you should keep a clean git history (and how to do it)"
description: "Keeping a clean git history is much more important than you might think. I show you why you should care and how you can do it."
date:        2019-11-25 14:34:00 +0100
---
An unclean git history isn't just ugly, it is not very helpful too. git provides a bunch of operations that could help you in a lot of situations, but using it only as a backup/rollback and deployment solution (and that happens way to often) will deny that advantage.

## Viewing specific changesets

Lets take a look at this example of a git history first, that could be part of a feature pull request branch that you want to review:

```
$ git log
f - Removed deprecated comment
e - Fixed some typos
d - Fix
c - Some needed testing framework changes
b - Fix
a - New feature
```

The first problem we face here is that we cannot dive into a changeset of a specific commit. Where would you place your `git show`? Where your `git log X..Y`? If you want to know what has happend to the testing framework you could take a look at commit `c`, but what if there are changes to the testing framework in commit `a` too?

You might say that it doesn't matter, because you just look at the complete changeset of the whole pull request. But lets skip ahead of time and create a scenario where these commits have been already merged into your main branches (like `master` for example) and you have to make a change to the testing framework again.

Now there is that one line in your codebase, that you couldn't make sense of. You ask why this line has been written that way. Of course, a well written comment might tell you why this line **is** necessary and why these specific arguments are needed for example. But what code and comments can't tell you is why a change to this line **was** necessary at all.

And that is because it depends on the context of the change and why specific commits are so important to a healthy codebase.

## Reverting specific changesets

The *fragmentation* problem for specific changes comes even more into account when you want to revert these changes. Lets assume that you want to revert the changes made to the testing framework, but you don't want to revert the feature itself. Now we have already said, that there is also testing framework code inside commit `a`, so using a *brainless* `git revert c` or `git revert a..c` isn't possible. Hell, `git revert` would be useless at all, if your git history has that problem. And that is sad. :(

## Rebasing interactivly

Now what we can do here is an interactive rebase of the branch. You might already know the process of rebasing ([in case you don't](https://git-scm.com/book/de/v1/Git-Branching-Rebasing)), but let me show you the process of interactive rebasing now.

With interactive rebasing we will rewrite our branch completly *interactive*. We can invoke that "mode" by calling `git rebase -i TARGET_BRANCH` where `TARGET_BRANCH` is the branch that you would merge your changes into (in our case it could be `origin/develop`). Executing this command will display your editor of choice (set by `$VISUAL`) filled with the log from above in a reversed order:

```
pick a New feature
pick b Fix
pick c Some needed testing framework changes
pick d Fix
pick e Fixed some typos
pick f Removed deprecated comment
```

Lets recall our goal here. We want one specific commit with all changes related to the feature itself and one specific commit with all changes related to the testing framework. We know that most of the changes to the testing framework are done in commit `c`, but that there are also some changes in commit `a`. The remaining commits are all related to the feature.

So what we can do here is exactly telling git, to *squash* the "remaining" commits into commit `a`, because that is where they belong. So instead of *picking* them, we want to squash them. And if you read the comment that git has put into your editor you will notice, that there is even a better option: `fixup`. So `squash`/`fixup` would take the changes of the commit and throw them into the parent commit (the one that is above the getting squashed commit). Lets do it (using `f` as a shorthand for `fixup`):

```
pick a New feature
f b Fix
pick c Some needed testing framework changes
f d Fix
f e Fixed some typos
f f Removed deprecated comment
```

But wait there is a problem here. When you read those *rewrite instructions* you notice, that we currently would squash the three belower commits into our commit `c` where our testing framework changes are living. To prevent this, we just have to move commit `c` below the other ones. It's that easy:


```
pick a New feature
f b Fix
f d Fix
f e Fixed some typos
f f Removed deprecated comment
pick c Some needed testing framework changes
```

When you now save and quit your editor git rebase will start working. It will go through all of your commits and apply the instruction that you gave it. When it hits some conflicts (for example when picking commit `c`, because there could be some changes at the same line in commit `f`, which have been made later before our rebase madness) it will stop proceeding and ask you to solve the conflicts, add/stage the new changes and coninue rebasing with `git rebase --continue`.

Your git history should look now like this:

```
$ git log
a - New feature
c - Some needed testing framework changes
```

Thats much more readable but I would love to give it better messages. Thats also achievable with `git rebase -i` and the `reword` instruction. It will open your editor of choice for every reword instruction. There you could rewrite your whole commit message.

```
$ git log
a - Implement a feature for creating a whole new fantasy game
c - Make it possible to call isolated tests inside the testing framework
```

Lovely. There is even more to choose good commit messages. A good resource for this is just [an example from Linus Torvalds](https://github.com/torvalds/subsurface-for-dirk/blob/a48494d2fbed58c751e9b7e8fbff88582f9b2d02/README#L88).

Now there is one more thing to do with `git rebase` and that is extracting the changes related to the testing framework from commit `a` into commit `c`. I really don't know if there is a comfortable way to it, but we can do it here by *editing* commit `a` and switching the position of the commit.

```
pick c Make it possible to call isolated tests inside the testing framework
e a Implement a feature for creating a whole new fantasy game
```

git rebase will stop there at `a` to let us do (*amend*) some changes, before continuing. We use this state to unstage all changes (`git reset HEAD`, where `HEAD` refers to commit `c`) and add them by picking only the testing framework related ones. We can achieve this by using `git add -p`. This command will guide us through every change made and ask us if we want to stage it. When we are done picking every testing framework change, we can stash the remaining changes (the feature implementation) with `git stash --keep-index`. You could even drop some bullshit changes there, that you might have made, so it's great to use `git add -p` everytime for stashing (and it even makes more fun when using the [singleKey config](https://git-scm.com/docs/git-config/1.6.2.1#git-config-interactivesinglekey)). Continue the rebase and start another one:

```
pick c Make it possible to call isolated tests inside the testing framework
f a Implement a feature for creating a whole new fantasy game
```

Notice that commit `a` now only holds our testing framework changes. After the squashing we are able to apply our stashed changes and recreate a purified version of the feature implementation commit: `git stash pop && git commit`.

## Bisecting for bad commits

Congratulations! Your branch is now clean, help- and beautiful and you can go on and merge it into your main branch.

But after some time of writing clean commits you notice that there is some bug in your main branch and you don't know how and **when** it has find its way into your codebase. And if you are not convinced to write clean commits yet that might be game changer for you, because finding that one bad commit (and it is only one, because of your good committing practice) is so damn simple now with `git bisect`.

`git bisect` lets you search your history via [a binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm). You can invoke the "mode" with `git bisect start` and tell it, that your current commit (the latest one) is a bad one, because it already holds the bug: `git bisect bad`.

Now you have to declare a commit range in which the **first** bad commit is present. You can do so by showing git bisect a good commit. Lets assume commit `a` is one in our main branch: `git bisect good a`

```
$ git log
f (bad) - Implement feature F
e - Implement feature E
d - Implement feature D
c - Implement feature C
b - Implement feature B
a (good) - Implement feature A
```

git bisect will now move to the commit in the middle and ask you to tell it, if this commit is a good or a bad one. By this approach it will find your first bad commit in `O(log n)` time in the average case. You could even provide it a command to automatically test a commit for bad behaviour with `git bisect run make test` for example. Go and grab a tea while git bisect finds your bug introducing commit all alone.

## Conclusio

I hope you understand now that writing unclean commits isn't just a matter of beauty and readability. You lose a great bunch of tools and operations that will keep your codebase healthy and your engineers happy.
