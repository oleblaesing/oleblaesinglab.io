---
layout:           external
title:            "Germany’s national healthcare system adopts Matrix!"
date:             2021-07-22 13:00:00 +0200
external:         https://matrix.org/blog/2021/07/21/germanys-national-healthcare-system-adopts-matrix
external_domain:  matrix.org
---
