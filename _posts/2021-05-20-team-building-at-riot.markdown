---
layout:           external
title:            "Building Self-Managed Teams: A Case Study from Riot Games"
date:             2021-05-20 10:14:00 +0200
external:         https://codingsans.com/blog/self-managed-teams
external_domain:  codingsans.com
---
