---
layout:      post
title:       "The hidden components pattern"
description: "Translating data structures into React elements is a problem, because it empathizes the usage of hidden components."
date:        2020-04-12 20:56:00 +0200
---
When you work with React, you're probably working in a functionalish way of programming.
This is what React encourages you to do and this is what you should do.
The problem is, that a team of developers that already has grown in the programming world,
will probably have an object-orientied background.

In a team like this, the functionalish approach can sometimes be counter-intuitive
and problems will be solved in a way, that React is not built for.
This is the reason why certain patterns (**not** design patterns) can emerge in such a React codebase.

## Translating data structures into React elements

One of this patterns I have seen commonly in my programming life,
is the translation of static data structures into React elements.
This operation is done by React components (functions that are returning React elements).

```jsx
const data = [
  { type: 'headline', value: 'My headline' },
  { type: 'paragraph', value: 'My paragraph' },
]

const Component = ({ type, value }) => {
  switch (type) {
    case 'headline':
      return <h1>{value}</h1>
    case 'paragraph':
      return <p>{value}</p>
  }
}

ReactDOM.render(data.map(({ type, value }) => (
  <Component
    type={type}
    value={value}
  />
)), document.body)
```

If you have an OOP background this kind of code might be familiar with you.
It's a [**factory function**](https://en.wikipedia.org/wiki/Factory_method_pattern)!

And the problem here is not the usage of a factory function itself (in this example it's just not worth it),
it's the usage of a `type` prop.

## An indicator for hidden components

Whenever you face a `type` prop in a React codebase,
chances are high that the component the prop is passed into has something,
that I wanna call *hidden components* in this post.

*Hidden components* are components that have been abstracted away
from any programmer, except the one, who has implemented them.
They will always be hidden behind a generic component name:

```jsx
<Generic type="a" href="/" />
```

This will be a problem, as soon as we need props
that will not be used by **every** component our generic component hides.

I would love to take a look at the HTML5 `input` element for this reason,
because it also uses a `type` attribute.

```html
<input
  type="text"
  max-length="100"
  required
/>

<input
  type="number"
  max="10"
  required
/>
```

You might notice that the name of the element is the same (`input`),
but the list of possible attributes is slightly different.
Which attribute you can use depends on the `type` attribute.

## One component to rule them all

Let's build an `Input` React component with these attributes:

```jsx
const Input = ({ type, maxLength, max, required }) => (
  <input
    type={type}
    maxLength={maxLength}
    max={max}
    required={required}
  />
)
```

... and use it:

```jsx
ReactDOM.render((
  <Input
    type="number"
    maxLength={100}
    required
  />
), document.body)
```

Aaand there is the first mistake we made while using our `Input` component.
According to the [HTML5 standard](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/number#Additional_attributes), number inputs can never have a `max-length` attribute.

But our component enables the programmer to use it this way.
How can we ensure, that this code won't find it's way into production?

We would have to check and harden it. To keep our example simple, we just check for the type `number` here:

```jsx
const Input = ({ type, maxLength, max, required }) => {
  const isNumber = type === 'number'

  return (
    <input
      type={type}
      maxLength={isNumber ? undefined : maxLength}
      max={isNumber ? max : undefined}
      required={required}
    />
  )
}
```

If we would have implemented all the other possible attributes of the input element,
*conditional rendering* (`type ? x : undefined`) would be the main thing, that straightly jumps to our face.
A nightmare to maintain and/or extend.

Major changes to components like these would nearly always be done by complete rewrites.
Now your manager will have nightmares too. :)

## Divide and conquer

We don't have the ability to ignore the HTML5 standard for the input element example,
but we can change the way our `Input` component behaves, by splitting it up.

```jsx
const TextInput = ({ maxLength, required }) => (
  <input
    type="text"
    maxLength={maxLength}
    required={required}
  />
)

const NumberInput = ({ max, required }) => (
  <input
    type="number"
    max={max}
    required={required}
  />
)
```

We could also build a shared interface for these components
(a technique that is being used a lot in the compound components design pattern):

```jsx
const Input = {
  Text: TextInput,
  Number: NumberInput,
}

ReactDOM.render(<Input.Number max={10} />, document.body)
```

Applying this approach already has solved our **static** data structure translation problem,
because instead of passing `type` props around,
we simply can (and should) just render the needed components directly:

```jsx
ReactDOM.render([
  <h1>My headline</h1>,
  <p>My paragraph</p>,
]), document.body)
```

If your data structure is not known by build time (because it's an API response or something),
this approach won't help you.
But in most cases you're better off not mixing different data types together.

So instead of sending this:

```json
[
  { "type": "p", "children": "My text" },
  { "type": "a", "href": "/" }
]
```

You should send something like this:

```json
{
  "paragraphs": [
    "My text"
  ],
  "links": [
    { "href": "/" }
  ]
}
```
