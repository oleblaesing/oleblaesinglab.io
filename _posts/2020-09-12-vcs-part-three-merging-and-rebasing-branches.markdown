---
layout:      post
title:       "VCS part three: Merging and rebasing branches"
description: "Merging and rebasing branches is the key functionality in our VCS. It enables you to be productive with it."
date:        2020-09-12 23:29:00 +0200
---
In [VCS part two: Reading and manipulating a repository](/2020/09/12/vcs-part-two-reading-and-manipulating-a-repository.html) of this series, we created our first functions to read and manipulate our repository. We did that by introducing the concept of branches, which we will now look closer at, as we're getting productive with our VCS.

## Merging

When you first learned about git, you probably only did a sequence of this commands for a certain time:

```sh
git status
git add .
git commit -m "probably meaningless message? shame on you!*"
git push
git checkout any-feature
git pull
git checkout master
git merge any-feature
```

*\* [Why you should keep a clean git history (and how to do it)](/2019/11/25/why-you-should-keep-a-clean-git-history-and-how-to-do-it.html)*

And if you were lucky enough to get introduced to `rebase`, then you mostly freaked out for the first time, right? That's kind of normal, but when you look at the implementation details for those two commands, you will, at least then, learn to love rebasing.

To make this effect even stronger, we will have to learn to fear `merge` first.

Lets create another "helper" function before we move on:

```js
function getDifferingCommits(ref) {
  const differingCommits = []
  let co = repository.branches[ref]

  while (co !== null) {
    if (co.hash === repository.HEAD.hash) {
      break
    }

    differingCommits.push(co)

    co = co.parent
  }

  // We never found a commit matching HEAD in the other branch
  if (co === null) {
    throw new Error('You can\'t differ already existing commits.')
  }

  // It's the exactly same state at the other branch as HEADs
  if (differingCommits.length === 0) {
    throw new Error('There are no commits differing.')
  }

  return differingCommits.reverse()
}
```

Btw. is "differing" a real word?

There is a problem with this code, because it just works with specific repository setups (target branch is always ahead, there are no commits on the `HEAD` branch etc.). But it will hopefully demonstrate how the diffing works in general here.

### Creating a new old commit

A merge won't manipulate your history in the first place, which is the reason git beginners prefer it to `rebase`. It will create a new commit made of all the other commits you would have merged. In a quick summary, merging uses this process:

1. Get the difference between the ref and the currently checked out branch (remember `HEAD`).
2. Go through all the changes of differing commits and *reduce* them into one, large changeset.
3. Create a new commit with this changeset and let `HEAD` point to it.

The implementation code looks like something like this:

```js
function merge(ref) {
  const changes = getDifferingCommits(ref).reduce((changeset, c) => {
    return `${changeset}${c.changes}`
  }, '')

  commit(`Merged ${ref} into ${getCheckedoutBranchName()}`, changes)
}
```

In reality, git keeps a record of every merged commit so you won't lose information about that, but in short theory, this is what `merge` does. It's ugly, because it's rough and destructive to a certain point.

## Rebasing

`rebase` on the other hand is way more straight forward, once you understand it. It just sets the differing commits on top of the `HEAD`. Yeah I know, every tutorial explains it this way and it doesn't really help a beginner, but maybe implementing `rebase` will?

```js
function rebase(ref) {
  repository.HEAD = getDifferingCommits(ref).reduce((parent, c) => {
    c.parent = parent
    c.hash = sha1(`${c.parent.hash}${c.changes}`)

    return c
  }, repository.HEAD)
}
```

Now what you are here looking at is again just plain ol' CS magic. You use the advantages of the linked list data structure to *rewrite* the history of the current branch/`HEAD`, by just re-pointing the parents.

Also, interesing use case for `Array.reduce` right? I bet pure functional programmers want to send me hate mails at this point.

## Where you can get further

Thats it for the short guide to teach people VCS by implementing one. I will go a lot deeper into detail and command options with my junior devs, but that's no the purpose of this post. Here, everything I wanted to show you is thinking of a giant data structure with pointers and functions to manipulate it, when you think of VCS. And I hope I succeeded with it. :)
