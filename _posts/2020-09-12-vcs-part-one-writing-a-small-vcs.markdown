---
layout:      post
title:       "VCS part one: Writing a small version control system"
description: "Writing a small version control system (VCS) is a good exercise to learn about it."
date:        2020-09-12 20:40:00 +0200
---
Right now, I'm in a position in my career where I spend a lot of time teaching and mentoring junior developers in their first months. And one exercise that really has shown off as a good way of teaching a lot of things (programming language features, data structures, version control systems etc.) is creating a version control system (VCS) from scratch. It might sound like an academic time waste in a non-academic business world, but it really pays off, because the usage of a VCS after that will raise in quality and speed.

Here, I want to give you a short guide on how to do it.

## Setting up the requirements

A fully featured VCS like git has a lot of responsibilities and tasks to handle in the modern software engineering world. It has to fetch respository headers from the network, apply file changes to your fileystem and give you nifty, interactive features to handle those changes (partial staging for example: `git add -p`). I want to focus on the primary feature a VCS has to handle and that is, well, handling versions or changesets as I will call it.

## How to describe changes

We can describe changes easily in a text format (you could use the program `diff` to do it for you):

```
---
add /hello.txt
1+hello world
---
modify /bye.txt
3-bye
3+ciao
---
delete /sub/ciao.txt
---
```

Each change is grouped together by a `---`. The first line of a change is a command of what has to be done.

1. Create a file named `hello.txt` and place it into the root directory of the repository.
2. Add the line "hello world" into the first line of this file.
3. Remove "bye" in the third line of the file `/bye.txt`.
4. Add the line "ciao" into the third line of this file.
5. Remove the file `/sub/ciao.txt`.

## On saving changes

The changeset alone won't benefit us that much, because major information is missing here. If you have multiple changesets and you want to create a local copy of them, you have to know the right order, because editing a file before it even has been created is not possible for example.

We could save a timestamp right next to the changeset, but if our VCS will be used in a decentral manner, it would be possible to create changesets at the exact same time. A better approach would be linking the previous changeset to the current one.

## An unique identifier

But before we can do that, we need some kind of identifier we can link our changeset to. We could use a regular counter for that purpose, but it will introduce problems like the one with the timestamp. A better approach is using hashes here, because we can easily hash our changeset and link the hash of the previous changeset:

```js
const changeset1 = {
  parent: null,
  changes: `
    ---
    add /hello.txt
    1+hello world
    ---
  `,
}

changeset1.hash = sha1(changeset1.changes)

const changeset2 = {
  parent: changeset1.hash,
  changes: `
    ---
    modify /hello.txt
    1-hello world
    1+hello vcs
    ---
  `,
}

changeset2.hash = sha1(changeset2.changes)
```

### Now with real uniqueness

There is another problem with this approach. In a grown project, some changes might be done multiple times over the years and they all would be creating the same hashes. The solution to this problem is super simple. Instead of just hashing the changes, we can also put the hash of the parent changeset as an input for the hash:

```js
changeset2.hash = sha1(`${changeset2.parent}${changeset2.changes}`)
```

Our hashes are now unique, no matter what. Only [hash collisions](https://en.wikipedia.org/wiki/Collision_(computer_science)) can stop us now, which is a topic for itself.

## Meta data

Our changesets are a bit too simple, because as a programmer you might want to know when a changeset has been created or by whom. Lets just add some simple meta data to our changeset and give them another name: **commit**.

```js
const commit = {
  parent: null,
  author: 'ob <ob@ob.codes>', // Read that from the `~/.gitconfig` config file
  date: new Date(),
  message: 'Initial commit',
  changes: `
    ---
    add /hello.txt
    1+hello world
    ---
  `,
}

commit.hash = sha1(`${commit.parent}${commit.changes}`)
```

## The birth of a repository

Storing each commit into its own variable might not be a flexible solution. We need some kind of data structure to store it. And luckily one of the first data structures you will learn of as a computer scientist seems to be a perfect fit for a simple VCS: [linked lists](https://en.wikipedia.org/wiki/Linked_list).

Summarized, a linked list is just a regular object like our commit, with a reference to another object of its kind. And guess what: we already have done this, just in a stupid way. The `parent` property of our commits is the perfect place to store the reference, instead of just the parent hash string.

We just have to store the information about the most current commit somewhere. I will call this variable `HEAD`. And we also have another new name in our VCS jargon: **repository**, a linked list made of commits.

```js
let HEAD

HEAD = {
  parent: null,
  author: 'ob <ob@ob.codes',
  date: new Date(),
  message: 'Initial commit',
  changes: `
    ---
    add /hello.txt
    1+hello world
    ---
  `,
}

HEAD.hash = sha1(`${HEAD.parent?.hash}${HEAD.changes}`)

HEAD = {
  parent: HEAD,
  author: 'ob <ob@ob.codes',
  date: new Date(),
  message: 'Second commit',
  changes: `
    ---
    modify /hello.txt
    1-hello world
    1+hello vcs
    ---
  `,
}

HEAD.hash = sha1(`${HEAD.parent?.hash}${HEAD.changes}`)
```

Lets sum that up in a simple JS function. The `commit` command you already know from git:

```js
// We assume that this has already been created
const repository = {
  HEAD: ...
}

function commit(message, changes) {
  const co = {
    parent: repository.HEAD,
    author: getAuthorFromConfig(),
    date: new Date(),
    message,
    changes: changes || getDiffFromFS(),
  }

  co.hash = sha1(`${co.parent?.hash}${co.changes}`)

  repository.HEAD = co
}
```

## Moving on

We now have a fully functioning data structure called repository with which we could work. There is just one thing missing: functions to work with it.

And here I have to say, that this is the thing I love most about git. It is just a giant but simple data structure which you can read out and manipulate with a small set of CLI commands (JS functions in our case).

If you are interested in implementing those commands you can read [VCS part two: Reading and manipulating a repository](/2020/09/12/vcs-part-two-reading-and-manipulating-a-repository.html) of this series.
